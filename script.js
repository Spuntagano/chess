(function($) {

	Chess = {

		grid : 8,
		tiles : [],

		Piece : function(x, y, color, alive, type){
			 this.x = x;
			 this.y = y;
			 this.color = color;
			 this.alive = alive;
			 this.type = type;
		},

		init : function(){
			var gridstr = "";
			gridstr += "<table>";
			for (var i=0; i<this.grid; i++){
				gridstr += "<tr>";
				for (var j=0; j<this.grid; j++){
					gridstr += "<td class='tile' id="+j+"-"+i+" data-x="+j+" data-y="+i+"></td>";
				}
				gridstr += "</tr>";
			}
			gridstr += "</table>"
			$("#wrapper").append(gridstr);

			for (var i=0; i<this.grid; i++){
				this.tiles[i] = new Array();
			}

			this.tiles[0][0] = new this.Piece(0,0,"black",true, "rook");
			this.tiles[7][0] = new this.Piece(7,0,"black",true, "rook");
			this.tiles[0][7] = new this.Piece(0,7,"white",true, "rook");
			this.tiles[7][7] = new this.Piece(7,7,"white",true, "rook");

			this.tiles[1][0] = new this.Piece(1,0,"black",true, "knight");
			this.tiles[6][0] = new this.Piece(6,0,"black",true, "knight");
			this.tiles[1][7] = new this.Piece(1,7,"white",true, "knight");
			this.tiles[6][7] = new this.Piece(6,7,"white",true, "knight");

			this.tiles[2][0] = new this.Piece(2,0,"black",true, "bishop");
			this.tiles[5][0] = new this.Piece(5,0,"black",true, "bishop");
			this.tiles[2][7] = new this.Piece(2,7,"white",true, "bishop");
			this.tiles[5][7] = new this.Piece(5,7,"white",true, "bishop");

			this.tiles[3][0] = new this.Piece(3,0,"black",true, "queen");
			this.tiles[3][7] = new this.Piece(3,7,"white",true, "queen");

			this.tiles[4][0] = new this.Piece(4,0,"black",true, "king");
			this.tiles[4][7] = new this.Piece(4,7,"white",true, "king");

			for (var i=0; i<this.grid; i++){
				for(var j=0; j<this.grid; j++){
					if (i == 1){
						this.tiles[j][i] = new this.Piece(j,1,"black",true, "pawn");
					}
					if (i == this.grid-2){
						this.tiles[j][i] = new this.Piece(j,this.grid-2,"white",true, "pawn");
					}
					if (this.tiles[j][i] == undefined){
						this.tiles[j][i] = "empty";
					}
				}
			}

			Chess.HTMLsync();

		},

		HTMLsync : function(){
			$(".white").removeClass("white");
			$(".black").removeClass("black");
			$("td").html("").unbind();
			for (i in Chess.tiles){
				if (Chess.tiles[i] != undefined){
					for (j in Chess.tiles[i]){
						if (Chess.tiles[i][j] != undefined){
							if (Chess.tiles[i][j].alive == true && Chess.tiles[i][j].type == "pawn"){
								Chess.syncPawn(Chess.tiles[i][j]);
								//console.log(this.tiles[i][j]);
							}
							if (Chess.tiles[i][j].alive == true && Chess.tiles[i][j].type == "rook"){
								Chess.syncRook(Chess.tiles[i][j]);
								//console.log(this.tiles[i][j]);
							}
							if (Chess.tiles[i][j].alive == true && Chess.tiles[i][j].type == "knight"){
								Chess.syncKnight(Chess.tiles[i][j]);
								//console.log(this.tiles[i][j]);
							}
							if (Chess.tiles[i][j].alive == true && Chess.tiles[i][j].type == "bishop"){
								Chess.syncBishop(Chess.tiles[i][j]);
								//console.log(this.tiles[i][j]);
							}
							if (Chess.tiles[i][j].alive == true && Chess.tiles[i][j].type == "queen"){
								Chess.syncQueen(Chess.tiles[i][j]);
								//console.log(this.tiles[i][j]);
							}
							if (Chess.tiles[i][j].alive == true && Chess.tiles[i][j].type == "king"){
								Chess.syncKing(Chess.tiles[i][j]);
								//console.log(this.tiles[i][j]);
							}
						}
					}
				}
			}
		},

		syncPawn : function(piece){
			Chess.getTileHTML(piece.x, piece.y).html("pawn").addClass("pawn");
			if (piece.color == "white"){
				Chess.getTileHTML(piece.x, piece.y).addClass("white");
			}
			if (piece.color == "black"){
				Chess.getTileHTML(piece.x, piece.y).addClass("black");
			}

			Chess.getTileHTML(piece.x, piece.y).on('click', function(){

				if (piece.color == "white"){
					Chess.getTileHTML(piece.x, piece.y-1).addClass("move");

					if (Chess.tiles[piece.x+1] != undefined){
						if (Chess.tiles[piece.x+1][piece.y-1] != undefined){
							if (Chess.tiles[piece.x+1][piece.y-1].color == "black" && Chess.tiles[piece.x+1][piece.y-1].alive){
								Chess.getTileHTML(piece.x+1,piece.y-1).addClass("kill");
							}
						}
					}

					if (Chess.tiles[piece.x-1] != undefined){
						if (Chess.tiles[piece.x-1][piece.y-1] != undefined){
							if (Chess.tiles[piece.x-1][piece.y-1].color == "black" && Chess.tiles[piece.x-1][piece.y-1].alive){
								Chess.getTileHTML(piece.x-1, piece.y-1).addClass("kill");
							}
						}
					}

					if (Chess.tiles[piece.x] != undefined){
						if (Chess.tiles[piece.x][piece.y-1] != undefined){
							if (Chess.tiles[piece.x][piece.y-1].alive){
								Chess.getTileHTML(piece.x, piece.y-1).removeClass("move");
							}
						}
					}

				}
				else if (piece.color == "black"){
					Chess.getTileHTML(piece.x, piece.y+1).addClass("move");

					if (Chess.tiles[piece.x+1] != undefined){
						if (Chess.tiles[piece.x+1][piece.y+1] != undefined){
							if (Chess.tiles[piece.x+1][piece.y+1].color == "white" && Chess.tiles[piece.x+1][piece.y+1].alive){
								Chess.getTileHTML(piece.x+1, piece.y+1).addClass("kill");
							}
						}
					}

					if (Chess.tiles[piece.x-1] != undefined){
						if (Chess.tiles[piece.x-1][piece.y+1] != undefined){
							if (Chess.tiles[piece.x-1][piece.y+1].color == "white" && Chess.tiles[piece.x-1][piece.y+1].alive){
								Chess.getTileHTML(piece.x-1, piece.y+1).addClass("kill");
							}
						}
					}

					if (Chess.tiles[piece.x] != undefined){
						if (Chess.tiles[piece.x][piece.y+1] != undefined){
							if (Chess.tiles[piece.x][piece.y+1].alive){
								Chess.getTileHTML(piece.x, piece.y+1).removeClass("move");
							}
						}
					}
				}
				$(".tile").on('click', function(){
					if ($(this).hasClass("move")){
						Chess.movePieces(piece, $(this).attr("data-x"), $(this).attr("data-y"));
					}
					else if ($(this).hasClass("kill")){
						Chess.destroyPieces(Chess.tiles[$(this).attr("data-x")][$(this).attr("data-y")]);
						Chess.movePieces(piece, $(this).attr("data-x"), $(this).attr("data-y"));
					}
					else{
						Chess.HTMLsync();
					}
					$(".move").removeClass("move");
					$(".kill").removeClass("kill");
				})

			})

		},

		syncRook : function(piece){
			Chess.getTileHTML(piece.x, piece.y).html("rook").addClass("rook");
			if (piece.color == "white"){
				Chess.getTileHTML(piece.x, piece.y).addClass("white");
			}
			if (piece.color == "black"){
				Chess.getTileHTML(piece.x, piece.y).addClass("black");
			}

			Chess.getTileHTML(piece.x, piece.y).on('click', function(){

				var i = piece.x;
				var j = piece.y;

				i++;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					i++;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}


				i = piece.x;
				j = piece.y;

				i--;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					i--;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}


				i = piece.x;
				j = piece.y;
				
				j++;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					j++;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}


				i = piece.x;
				j = piece.y;

				j--;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					j--;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}


				$(".tile").on('click', function(){
					if ($(this).hasClass("move")){
						Chess.movePieces(piece, $(this).attr("data-x"), $(this).attr("data-y"));
					}
					else if ($(this).hasClass("kill")){
						Chess.destroyPieces(Chess.tiles[$(this).attr("data-x")][$(this).attr("data-y")]);
						Chess.movePieces(piece, $(this).attr("data-x"), $(this).attr("data-y"));
					}
					else{
						Chess.HTMLsync();
					}
					$(".move").removeClass("move");
					$(".kill").removeClass("kill");
				})
					

			})

		},

		syncKnight : function(piece){
			Chess.getTileHTML(piece.x, piece.y).html("knight").addClass("knight");
			if (piece.color == "white"){
				Chess.getTileHTML(piece.x, piece.y).addClass("white");
			}
			if (piece.color == "black"){
				Chess.getTileHTML(piece.x, piece.y).addClass("black");
			}

			Chess.getTileHTML(piece.x, piece.y).on('click', function(){

				if (Chess.tiles[piece.x+2] != undefined){
					if (Chess.tiles[piece.x+2][piece.y+1] != undefined){
						if (Chess.tiles[piece.x+2][piece.y+1] == "empty"){
							Chess.getTileHTML(piece.x+2, piece.y+1).addClass("move");
						}
						else if (Chess.tiles[piece.x+2][piece.y+1].color != piece.color){
							Chess.getTileHTML(piece.x+2, piece.y+1).addClass("kill");
						}
					}
				}

				if (Chess.tiles[piece.x+1] != undefined){
					if (Chess.tiles[piece.x+1][piece.y+2] != undefined){
						if (Chess.tiles[piece.x+1][piece.y+2] == "empty"){
							Chess.getTileHTML(piece.x+1, piece.y+2).addClass("move");
						}
						else if (Chess.tiles[piece.x+1][piece.y+2].color != piece.color){
							Chess.getTileHTML(piece.x+1, piece.y+2).addClass("kill");
						}
					}
				}

				if (Chess.tiles[piece.x-2] != undefined){
					if (Chess.tiles[piece.x-2][piece.y-1] != undefined){
						if (Chess.tiles[piece.x-2][piece.y-1] == "empty"){
							Chess.getTileHTML(piece.x-2, piece.y-1).addClass("move");
						}
						else if (Chess.tiles[piece.x-2][piece.y-1].color != piece.color){
							Chess.getTileHTML(piece.x-2, piece.y-1).addClass("kill");
						}
					}
				}

				if (Chess.tiles[piece.x-1] != undefined){
					if (Chess.tiles[piece.x-1][piece.y-2] != undefined){
						if (Chess.tiles[piece.x-1][piece.y-2] == "empty"){
							Chess.getTileHTML(piece.x-1, piece.y-2).addClass("move");
						}
						else if (Chess.tiles[piece.x-1][piece.y-2].color != piece.color){
							Chess.getTileHTML(piece.x-1, piece.y-2).addClass("kill");
						}
					}
				}

				if (Chess.tiles[piece.x-2] != undefined){
					if (Chess.tiles[piece.x-2][piece.y+1] != undefined){
						if (Chess.tiles[piece.x-2][piece.y+1] == "empty"){
							Chess.getTileHTML(piece.x-2, piece.y+1).addClass("move");
						}
						else if (Chess.tiles[piece.x-2][piece.y+1].color != piece.color){
							Chess.getTileHTML(piece.x-2, piece.y+1).addClass("kill");
						}
					}
				}

				if (Chess.tiles[piece.x-1] != undefined){
					if (Chess.tiles[piece.x-1][piece.y+2] != undefined){
						if (Chess.tiles[piece.x-1][piece.y+2] == "empty"){
							Chess.getTileHTML(piece.x-1, piece.y+2).addClass("move");
						}
						else if (Chess.tiles[piece.x-1][piece.y+2].color != piece.color){
							Chess.getTileHTML(piece.x-1, piece.y+2).addClass("kill");
						}
					}
				}

				if (Chess.tiles[piece.x+2] != undefined){
					if (Chess.tiles[piece.x+2][piece.y-1] != undefined){
						if (Chess.tiles[piece.x+2][piece.y-1] == "empty"){
							Chess.getTileHTML(piece.x+2, piece.y-1).addClass("move");
						}
						else if (Chess.tiles[piece.x+2][piece.y-1].color != piece.color){
							Chess.getTileHTML(piece.x+2, piece.y-1).addClass("kill");
						}
					}
				}

				if (Chess.tiles[piece.x+1] != undefined){
					if (Chess.tiles[piece.x+1][piece.y-2] != undefined){
						if (Chess.tiles[piece.x+1][piece.y-2] == "empty"){
							Chess.getTileHTML(piece.x+1, piece.y-2).addClass("move");
						}
						else if (Chess.tiles[piece.x+1][piece.y-2].color != piece.color){
							Chess.getTileHTML(piece.x+1, piece.y-2).addClass("kill");
						}
					}
				}


				$(".tile").on('click', function(){
					if ($(this).hasClass("move")){
						Chess.movePieces(piece, $(this).attr("data-x"), $(this).attr("data-y"));
					}
					else if ($(this).hasClass("kill")){
						Chess.destroyPieces(Chess.tiles[$(this).attr("data-x")][$(this).attr("data-y")]);
						Chess.movePieces(piece, $(this).attr("data-x"), $(this).attr("data-y"));
					}
					else{
						Chess.HTMLsync();
					}
					$(".move").removeClass("move");
					$(".kill").removeClass("kill");
				})
					

			})

		},

		syncBishop : function(piece){
			Chess.getTileHTML(piece.x, piece.y).html("bishop").addClass("bishop");
			if (piece.color == "white"){
				Chess.getTileHTML(piece.x, piece.y).addClass("white");
			}
			if (piece.color == "black"){
				Chess.getTileHTML(piece.x, piece.y).addClass("black");
			}

			Chess.getTileHTML(piece.x, piece.y).on('click', function(){


				var i = piece.x;
				var j = piece.y;

				i++;
				j++;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					i++;
					j++;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}


				i = piece.x;
				j = piece.y;

				i--;
				j--;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					i--;
					j--;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}


				i = piece.x;
				j = piece.y;
				
				i--;
				j++;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					i--;
					j++;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}


				i = piece.x;
				j = piece.y;

				i++
				j--;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					i++;
					j--;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}


				$(".tile").on('click', function(){
					if ($(this).hasClass("move")){
						Chess.movePieces(piece, $(this).attr("data-x"), $(this).attr("data-y"));
					}
					else if ($(this).hasClass("kill")){
						Chess.destroyPieces(Chess.tiles[$(this).attr("data-x")][$(this).attr("data-y")]);
						Chess.movePieces(piece, $(this).attr("data-x"), $(this).attr("data-y"));
					}
					else{
						Chess.HTMLsync();
					}
					$(".move").removeClass("move");
					$(".kill").removeClass("kill");
				})
					

			})

		},

		syncQueen : function(piece){
			Chess.getTileHTML(piece.x, piece.y).html("queen").addClass("queen");
			if (piece.color == "white"){
				Chess.getTileHTML(piece.x, piece.y).addClass("white");
			}
			if (piece.color == "black"){
				Chess.getTileHTML(piece.x, piece.y).addClass("black");
			}

			Chess.getTileHTML(piece.x, piece.y).on('click', function(){


				var i = piece.x;
				var j = piece.y;

				i++;
				j++;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					i++;
					j++;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}


				i = piece.x;
				j = piece.y;

				i--;
				j--;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					i--;
					j--;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}


				i = piece.x;
				j = piece.y;
				
				i--;
				j++;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					i--;
					j++;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}


				i = piece.x;
				j = piece.y;

				i++;
				j--;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					i++;
					j--;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}

				i = piece.x;
				j = piece.y;

				i++;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					i++;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}


				i = piece.x;
				j = piece.y;

				i--;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					i--;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}


				i = piece.x;
				j = piece.y;
				
				j++;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					j++;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}


				i = piece.x;
				j = piece.y;

				j--;
				while(Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && Chess.tiles[i][j] == "empty"){
					Chess.getTileHTML(i, j).addClass("move");
					j--;
				}
				if (Chess.tiles[i] != undefined && Chess.tiles[i][j] != undefined && piece.color != Chess.tiles[i][j].color){
					Chess.getTileHTML(i, j).addClass("kill");
				}


				$(".tile").on('click', function(){
					if ($(this).hasClass("move")){
						Chess.movePieces(piece, $(this).attr("data-x"), $(this).attr("data-y"));
					}
					else if ($(this).hasClass("kill")){
						Chess.destroyPieces(Chess.tiles[$(this).attr("data-x")][$(this).attr("data-y")]);
						Chess.movePieces(piece, $(this).attr("data-x"), $(this).attr("data-y"));
					}
					else{
						Chess.HTMLsync();
					}
					$(".move").removeClass("move");
					$(".kill").removeClass("kill");
				})
					

			})

		},

		syncKing : function(piece){
			Chess.getTileHTML(piece.x, piece.y).html("king").addClass("king");
			if (piece.color == "white"){
				Chess.getTileHTML(piece.x, piece.y).addClass("white");
			}
			if (piece.color == "black"){
				Chess.getTileHTML(piece.x, piece.y).addClass("black");
			}

			Chess.getTileHTML(piece.x, piece.y).on('click', function(){

				i = piece.x;
				j = piece.y;


				if (Chess.tiles[i+1] != undefined && Chess.tiles[i+1][j] != undefined){
					if (Chess.tiles[i+1][j] == "empty"){
						Chess.getTileHTML(i+1, j).addClass("move");
					}
					else if (Chess.tiles[i+1][j].color != piece.color){
						Chess.getTileHTML(i+1, j).addClass("kill");
					}
				}

				if (Chess.tiles[i+1] != undefined && Chess.tiles[i+1][j+1] != undefined){
					if (Chess.tiles[i+1][j+1] == "empty"){
						Chess.getTileHTML(i+1, j+1).addClass("move");
					}
					else if (Chess.tiles[i+1][j+1].color != piece.color){
						Chess.getTileHTML(i+1, j+1).addClass("kill");
					}
				}

				if (Chess.tiles[i] != undefined && Chess.tiles[i][j+1] != undefined){
					if (Chess.tiles[i][j+1] == "empty"){
						Chess.getTileHTML(i, j+1).addClass("move");
					}
					else if (Chess.tiles[i][j+1].color != piece.color){
						Chess.getTileHTML(i, j+1).addClass("kill");
					}
				}

				if (Chess.tiles[i-1] != undefined && Chess.tiles[i-1][j] != undefined){
					if (Chess.tiles[i-1][j] == "empty"){
						Chess.getTileHTML(i-1, j).addClass("move");
					}
					else if (Chess.tiles[i-1][j].color != piece.color){
						Chess.getTileHTML(i-1, j).addClass("kill");
					}
				}

				if (Chess.tiles[i-1] != undefined && Chess.tiles[i-1][j+1] != undefined){
					if (Chess.tiles[i-1][j+1] == "empty"){
						Chess.getTileHTML(i-1, j+1).addClass("move");
					}
					else if (Chess.tiles[i-1][j+1].color != piece.color){
						Chess.getTileHTML(i-1, j+1).addClass("kill");
					}
				}

				if (Chess.tiles[i-1] != undefined && Chess.tiles[i-1][j-1] != undefined){
					if (Chess.tiles[i-1][j-1] == "empty"){
						Chess.getTileHTML(i-1, j-1).addClass("move");
					}
					else if (Chess.tiles[i-1][j-1].color != piece.color){
						Chess.getTileHTML(i-1, j-1).addClass("kill");
					}
				}

				if (Chess.tiles[i+1] != undefined && Chess.tiles[i+1][j-1] != undefined){
					if (Chess.tiles[i+1][j-1] == "empty"){
						Chess.getTileHTML(i+1, j-1).addClass("move");
					}
					else if (Chess.tiles[i+1][j-1].color != piece.color){
						Chess.getTileHTML(i+1, j-1).addClass("kill");
					}
				}

				if (Chess.tiles[i] != undefined && Chess.tiles[i][j-1] != undefined){
					if (Chess.tiles[i][j-1] == "empty"){
						Chess.getTileHTML(i, j-1).addClass("move");
					}
					else if (Chess.tiles[i][j-1].color != piece.color){
						Chess.getTileHTML(i, j-1).addClass("kill");
					}
				}

				$(".tile").on('click', function(){
					if ($(this).hasClass("move")){
						Chess.movePieces(piece, $(this).attr("data-x"), $(this).attr("data-y"));
					}
					else if ($(this).hasClass("kill")){
						Chess.destroyPieces(Chess.tiles[$(this).attr("data-x")][$(this).attr("data-y")]);
						Chess.movePieces(piece, $(this).attr("data-x"), $(this).attr("data-y"));
					}
					else{
						Chess.HTMLsync();
					}
					$(".move").removeClass("move");
					$(".kill").removeClass("kill");
				})
					

			})

		},

		movePieces : function(piece, x, y){
			this.tiles[piece.x][piece.y] = "empty";
			piece.x = parseInt(x, 10);
			piece.y = parseInt(y, 10);
			this.tiles[piece.x][piece.y] = piece;
			this.HTMLsync();
		},

		destroyPieces : function(piece){
			piece.alive = false;
			this.HTMLsync;
		},

		getTileHTML : function(x, y){
			return $("#"+x+"-"+y);
		}

	}


})(jQuery);

Chess.init();